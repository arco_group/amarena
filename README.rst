
Due a test suite like next::

 Suite Foo:
   Case A:
     Test 1: OK
     Test 2: FAIL
   Case B:
     Test 3: ERROR

worker notifies::

 notifySuiteInfo('/path/to/Foo', START)
 notifyCaseInfo('Foo', 'A', START)
 notifyTestInfo('Foo', 'A',  '1', START)
 notifyTestInfo('Foo', 'A',  '1', OK)
 notifyTestInfo('Foo', 'A',  '2', START)
 notifyTestInfo('Foo', 'A',  '2', FAIL)
 notifyCaseInfo('Foo', 'B', START)
 notifyTestInfo('Foo', 'B',  '3', START)
 notifyTestInfo('Foo', 'B',  '3', ERROR)
 notifySuiteInfo('/path/to/Foo', STOP)
