# -*- mode: makefile-gmake; coding: utf-8 -*-

APP = atheist2
VERSION = $(shell head -n 1 debian/changelog | cut -f2 -d " " | tr -d "()" | cut -f1 -d "-")

tests:
	@find test -name "*.py" | xargs nosetests

clean: ACTION = -delete -printf "Remove file: '\033[01;31m%p\033[00m'\n"
clean:
	@find . -name "*.pyc" $(ACTION)
	@find . -name "*.o" $(ACTION)
	@find . -name "*~" $(ACTION)
	@find . -name "atheist.cc" $(ACTION)
	@find . -name "atheist.h" $(ACTION)
	@find . -name "atheist-gtest-launcher" $(ACTION)
	@-$(RM) debian/python-module-stampdir/ build -rf

