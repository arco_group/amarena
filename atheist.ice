// -*- mode: c++; coding: utf-8 -*-

module Atheist {

    exception UnknownSuiteException {
	string name;
    };

    enum StatusID {START, STOP, FAIL, OK, NOEXEC, ERROR, UNKNOWN, RUNNING, TODO};
    enum OutputType {STDOUT, STDERR, SETUPOUT, SETUPERR};

    struct SuiteInfo {
	string name;
	StatusID status;
    };

    struct CaseInfo {
	string suiteName;
	string name;
	StatusID status;
    };

    struct TestInfo {
	string suiteName;
	string caseName;
	string name;
	StatusID status;
    };

    struct SuiteOutputData {
	string name;
	OutputType type;
	string data;
    };

    interface Reporter {
	void notifySuiteInfo(SuiteInfo info);
	void notifySuiteOutput(SuiteOutputData data);

	void notifyCaseInfo(CaseInfo info);
	void notifyTestInfo(TestInfo info);

	StatusID getStatus();
    };

    interface Worker {
	void run();
    };

    interface WorkerFactory {
	bool canHandle(string suiteURI);
    };
};
