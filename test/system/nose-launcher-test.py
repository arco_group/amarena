# -*- coding:utf-8; tab-width:4; mode:python -*-

from unittest import TestCase

import Ice

Ice.loadSlice('atheist.ice')

import Atheist

import prego
from doublex import *
from hamcrest.library.object.hasproperty import *

from commodity.os_ import SubProcess, check_output, CalledProcessError


class NoseLauncherTests(TestCase):
    def setUp(self):
        self.servant = Mimic(Spy, Atheist.Reporter)
        ic = Ice.initialize()
        adapter = ic.createObjectAdapterWithEndpoints('adapter', 'tcp')
        adapter.activate()
        self.reporter = adapter.addWithUUID(self.servant)

    def run_launcher(self, suite, cwd='.'):
        prego.init()
        test = prego.Test(suite)
        test.set_cmd("atheist2/worker/nose/nose_launcher.py --cwd {0} {1} '{2}'".format(
                cwd, suite, self.reporter))
        prego.verify()

    def test_ok(self):
        self.run_launcher('test/samples/test_ok.py')

        assert_that(self.servant.notifyTestInfo, called())
        assert_that(self.servant.notifyTestInfo,
                    called_with(has_property('status', Atheist.StatusID.OK),
                                ANY_ARG))

    def test_cwd_ok(self):
        self.run_launcher('samples/test_ok.py', 'test')

        assert_that(self.servant.notifyTestInfo, called())
        assert_that(self.servant.notifyTestInfo,
                    called_with(has_property('status', Atheist.StatusID.OK),
                                ANY_ARG))

    def test_fail(self):
        self.run_launcher('test/samples/test_fail.py')

        assert_that(self.servant.notifyTestInfo, called())
        assert_that(self.servant.notifyTestInfo,
                    called_with(has_property('status', Atheist.StatusID.FAIL),
                                ANY_ARG))
