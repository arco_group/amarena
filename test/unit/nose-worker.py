# -*- coding:utf-8; mode:python -*-

import os
from unittest import TestCase
import tempfile

import Ice

from fs.memoryfs import MemoryFS
from fs.osfs import OSFS
from doublex import Spy, ProxySpy, ANY_ARG, called, called_with, Mimic

from hamcrest import *
from hamcrest.library.object.hasproperty import *

from atheist2.reporter import Reporter, ConsoleReporter, DebugReporter
from atheist2.worker import LocalWorkerFactory, CantHandleURI
from atheist2.worker.base import WorkerFactoryBase, Worker
from atheist2.worker.worker_nose import NoseWorker

import Atheist
from Atheist import StatusID as st


class TestNoseFactory(TestCase):
    def setUp(self):
        self.fs = MemoryFS()
        WorkerFactoryBase.fs = self.fs
        Worker.fs = self.fs

    def test_factory_can_not_handle_not_dot_py_files(self):
        uri = "some.nopy"
        self.fs.createfile(uri)
        with self.assertRaises(CantHandleURI):
            LocalWorkerFactory.create(uri, None)

    def test_local_factory_can_create_PyUnitWorker(self):
        uri = "some.py"
        self.fs.createfile(uri)
        worker = LocalWorkerFactory.create(uri, None)

        self.assertTrue(isinstance(worker, NoseWorker))


class TestNoseWorker(TestCase):
    def setUp(self):
        Worker.fs = OSFS('/')
        self.reporter = Mimic(Spy, Atheist.Reporter)
        self.reporter_proxy = self.get_reporter(self.reporter)

    def get_reporter(self, servant):
        ic = Ice.initialize()
        adapter = ic.createObjectAdapterWithEndpoints("Adapter", "tcp")
        adapter.activate()
        reporter_proxy = adapter.addWithUUID(servant)
        reporter_proxy = Atheist.ReporterPrx.uncheckedCast(reporter_proxy)
        return reporter_proxy

    def exec_pyunit(self, content):
        with tempfile.NamedTemporaryFile(suffix='.py', delete=False) as tmp:
            tmp.write(content)
            tmp.flush()

            worker = NoseWorker(tmp.name, self.reporter_proxy)
            worker.run()

    def test_trivial_test_ok(self):
        self.exec_pyunit('''\
import unittest
class Case1(unittest.TestCase):
    def test_ok(self):
        pass
''')

        assert_that(self.reporter.notifySuiteInfo, called())
        assert_that(self.reporter.notifyTestInfo,
                    called_with(has_property('status', st.OK), ANY_ARG))

    def test_trivial_test_fail(self):
        self.exec_pyunit('''\
import unittest
class Case1(unittest.TestCase):
    def test_fail(self):
        self.fail()
''')

        assert_that(self.reporter.notifySuiteInfo, called())
        assert_that(self.reporter.notifyTestInfo,
                    called_with(has_property('status', st.FAIL), ANY_ARG))

    def test_2_tests(self):
        self.exec_pyunit('''\
import unittest
class Case1(unittest.TestCase):
    def test_ok1(self):
        pass
    def test_ok2(self):
        pass
''')

        assert_that(self.reporter.notifySuiteInfo, called())
        assert_that(self.reporter.notifyTestInfo,
                    called_with(has_property('status', st.OK), ANY_ARG).times(2))

    def test_2_cases(self):
        self.exec_pyunit('''\
import unittest
class Case1(unittest.TestCase):
    def test_ok1(self):
        pass
class Case2(unittest.TestCase):
    def test_ok2(self):
        pass
''')

        assert_that(self.reporter.notifySuiteInfo, called())
        assert_that(self.reporter.notifyTestInfo,
                    called_with(has_property('status', st.OK), ANY_ARG).times(2))
