# -*- coding:utf-8; mode:python -*-

from unittest import TestCase
import Ice

from atheist2.reporter import Reporter, ConsoleReporter
import Atheist

START = Atheist.StatusID.START
STOP = Atheist.StatusID.STOP
OK = Atheist.StatusID.OK
FAIL = Atheist.StatusID.FAIL
ERROR = Atheist.StatusID.ERROR
STDOUT = Atheist.OutputType.STDOUT


class ReporterHelper:
    def __init__(self, reporter):
        self.reporter = reporter

    def do_suite_start(self, id):
        info = Atheist.SuiteInfo(id, START)
        self.reporter.notifySuiteInfo(info)

    def do_suite_stop(self, id):
        info = Atheist.SuiteInfo(id, STOP)
        self.reporter.notifySuiteInfo(info)

    def do_suite_error(self, id):
        info = Atheist.SuiteInfo(id, ERROR)
        self.reporter.notifySuiteInfo(info)

    def do_suite_output(self, id, type, data):
        data = Atheist.SuiteOutputData(id, type, data)
        self.reporter.notifySuiteOutput(data)

    def do_case_start(self, id):
        suite, name = id.split(":")
        info = Atheist.CaseInfo(suite, name, START)
        self.reporter.notifyCaseInfo(info)

    def do_case_ok(self, id):
        suite, name = id.split(":")
        info = Atheist.CaseInfo(suite, name, OK)
        self.reporter.notifyCaseInfo(info)

    def do_case_fail(self, id):
        suite, name = id.split(":")
        info = Atheist.CaseInfo(suite, name, FAIL)
        self.reporter.notifyCaseInfo(info)

    def do_test_start(self, id):
        suite, case, name = id.split(":")
        info = Atheist.TestInfo(suite, case, name, START)
        self.reporter.notifyTestInfo(info)

    def do_test_fail(self, id):
        suite, case, name = id.split(":")
        info = Atheist.TestInfo(suite, case, name, FAIL)
        self.reporter.notifyTestInfo(info)


class TestReporter(TestCase):
    def test_dont_mix_cases_from_different_suites(self):
        # given
        sut = Reporter()
        helper = ReporterHelper(sut)

        # when
        helper.do_suite_start ("suite1")
        helper.do_suite_start ("suite2")
        helper.do_case_start  ("suite1:case1")
        helper.do_case_start  ("suite2:case3")
        helper.do_case_start  ("suite1:case2")

        # then
        self.assertEquals(["suite1", "suite2"], sut.suites.keys())
        self.assertEquals(["case1", "case2"],  sut.suites['suite1'].keys())
        self.assertEquals(["case3"], sut.suites['suite2'].keys())

    def test_dont_mix_test_from_different_cases(self):
        # given
        sut = Reporter()
        helper = ReporterHelper(sut)

        # when
        helper.do_suite_start ("suite1")
        helper.do_case_start  ("suite1:case1")
        helper.do_case_start  ("suite1:case2")
        helper.do_test_start  ("suite1:case1:test1")
        helper.do_test_start  ("suite1:case2:test3")
        helper.do_test_start  ("suite1:case1:test2")

        # then
        suite = sut.suites['suite1']
        self.assertEquals(["case1", "case2"], suite.keys())
        self.assertEquals(["test1", "test2"], suite.get_case('case1').keys())
        self.assertEquals(["test3"], suite.get_case('case2').keys())

    def test_update_suite_info(self):
        # given
        sut = Reporter()
        helper = ReporterHelper(sut)

        # when
        helper.do_suite_start ("suite1")
        helper.do_case_ok     ("suite1:case1")
        helper.do_suite_stop  ("suite1")

        # then
        self.assertEquals(sut.suites['suite1'].get_case('case1').name, "case1")

    def test_get_status_all_ok(self):
        # given
        sut = Reporter()
        helper = ReporterHelper(sut)

        # when
        helper.do_suite_start ("suite1")
        helper.do_case_start  ("suite1:case1")
        helper.do_case_ok     ("suite1:case1")
        helper.do_suite_stop  ("suite1")

        # then
        self.assertEquals(sut.getStatus(), OK)

    def test_get_status_some_fails(self):
        # given
        sut = Reporter()
        helper = ReporterHelper(sut)

        # when
        helper.do_suite_start ("suite1")
        helper.do_case_start  ("suite1:case1")
        helper.do_test_fail   ("suite1:case1:test1")
        helper.do_suite_stop  ("suite1")

        # then
        self.assertEquals(sut.getStatus(), FAIL)

    def test_get_status_when_an_error_ocurred(self):
        # given
        sut = Reporter()
        helper = ReporterHelper(sut)

        # when
        helper.do_suite_start("suite1")
        helper.do_suite_error("suite1")

        # then
        self.assertEquals(sut.getStatus(), ERROR)


class TestReporterOutput(TestCase):
    def test_suite_setup_output(self):
        sut = Reporter()
        helper = ReporterHelper(sut)
        out = "There were some output"

        helper.do_suite_start("suite1")
        helper.do_suite_output("suite1", STDOUT, out)

        self.assertEquals(sut.suites['suite1'].get_output(STDOUT), out)

    def test_suite_setup_output_of_unstarted_suite(self):
        sut = Reporter()
        helper = ReporterHelper(sut)
        out = "There were some output"

        self.assertRaises(
            Atheist.UnknownSuiteException,
            helper.do_suite_output, "suite1", STDOUT, out)


class TestConsoleReporterRenderer(TestCase):
    def test_print_suite_and_cases_info(self):
        # given
        sut = ConsoleReporter()
        helper = ReporterHelper(sut)

        # when
        helper.do_suite_start ("suite1")
        helper.do_case_start  ("suite1:case1")
        helper.do_test_start  ("suite1:case1:test1")
        result = sut.render()

        # then
        self.assertIn("suite1", result.split())
        self.assertIn("case1", result.split())
        self.assertIn("test1", result.split())
