# -*- mode:python; coding:utf-8; tab-width:4 -*-

from unittest import TestCase

from fs.memoryfs import MemoryFS

from atheist2.worker.base import WorkerFactoryBase
from atheist2.worker import NoseFactory, GTestFactory


class TestGtestFactory(TestCase):
    def setUp(self):
        self.fs = MemoryFS()
        WorkerFactoryBase.fs = self.fs
        self.factory = GTestFactory()

    def test_can_handle_gtest_dir(self):
        uri = "gtest"
        self.fs.makedir(uri)
        self.fs.createfile("{0}/atheist.mk".format(uri))
        self.fs.createfile("{0}/test.cc".format(uri))
        self.assertTrue(self.factory.canHandle(uri))

    def test_can_not_handle_file(self):
        uri = "non-gtest"
        self.fs.createfile(uri)
        self.assertFalse(self.factory.canHandle(uri))

    def test_can_not_handle_wrong_dir(self):
        uri = "non-gtest-dir"
        self.fs.makedir(uri)
        self.fs.createfile("{0}/hello.py".format(uri))

        self.assertFalse(self.factory.canHandle(uri))


class TestNoseFactory(TestCase):
    def setUp(self):
        self.fs = MemoryFS()
        WorkerFactoryBase.fs = self.fs

    def test_can_handle_python_file(self):
        uri = "test.py"
        self.fs.createfile(uri)
        factory = NoseFactory()
        self.assertTrue(factory.canHandle(uri))

    def test_can_not_handle_inexistent_file(self):
        uri = "test.py"
        factory = NoseFactory()
        self.assertFalse(factory.canHandle(uri))
