# -*- coding:utf-8; mode:python -*-

from unittest import TestCase

from fs.memoryfs import MemoryFS
from pyDoubles.framework import spy, assert_that_method
import Ice

from atheist2.worker import LocalWorkerFactory
from atheist2.worker.base import WorkerFactoryBase, Worker
import Atheist


class TestPyunitWorker(TestCase):
    def setUp(self):
        uri = "hello-test.py"

        fs = MemoryFS()
        fs.createfile(uri)

        print id(fs)
        WorkerFactoryBase.fs = fs
        Worker.fs = fs

        self.reporter = spy(Atheist.Reporter)
        servant = LocalWorkerFactory.create(uri, self.reporter)

        ic = Ice.initialize()
        adapter = ic.createObjectAdapterWithEndpoints("adapter", "tcp")
        prx = adapter.addWithUUID(servant)
        self.worker_prx = Atheist.WorkerPrx.uncheckedCast(prx)
        adapter.activate()

    def test_on_worker_start_notify_suite_running(self):
        # when
        self.worker_prx.run()

        # then
        assert_that_method(self.reporter.notifyCaseInfo).was_called()
