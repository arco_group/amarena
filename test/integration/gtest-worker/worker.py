# -*- coding:utf-8; mode:python -*-

import os

import Ice
from unittest import TestCase
from doublex import *
from hamcrest import *
from fs.memoryfs import MemoryFS

from atheist2.worker.base import Worker
from atheist2.worker.worker_gtest import GTestWorker
import Atheist


class ReporterI(Atheist.Reporter):
    def __init__(self, delegated):
        self.delegated = delegated

    def notifySuiteInfo(self, info, current):
        print "SUITE INFO: ", info.name, info.status
        self.delegated.notifySuiteInfo(info)

    def notifySuiteOutput(self, data, current):
        print "SUITE OUTPUT: ", data.name
        for line in data.data.splitlines():
            print "   ->", line
        self.delegated.notifySuiteOutput(data)

    def notifyCaseInfo(self, info, current):
        print "CASE INFO:", info.name, info.status
        self.delegated.notifyCaseInfo(info)

    def notifyTestInfo(self, info, current):
        print "TEST INFO:", info.name, info.status
        self.delegated.notifyTestInfo(info)


class TestBase(TestCase):
    def get_reporter_proxy(self, reporter):
        servant = ReporterI(reporter)
        ic = Ice.initialize()
        adapter = ic.createObjectAdapterWithEndpoints("Adapter", "tcp")
        adapter.activate()
        reporter_proxy = adapter.addWithUUID(servant)
        return Atheist.ReporterPrx.uncheckedCast(reporter_proxy)


class TestWorkerNotifications(TestBase):
    def setUp(self):
        self.fs = MemoryFS()

    def build_infos(self, uri):
        self.suite_start = Atheist.SuiteInfo(uri, Atheist.StatusID.START)
        self.suite_stop  = Atheist.SuiteInfo(uri, Atheist.StatusID.STOP)
        self.suite_error = Atheist.SuiteInfo(uri, Atheist.StatusID.ERROR)

        self.case_start = Atheist.CaseInfo(
            uri, 'GTestCase', Atheist.StatusID.START)
        self.case_ok = Atheist.CaseInfo(
            uri, 'GTestCase', Atheist.StatusID.OK)
        self.case_fail = Atheist.CaseInfo(
            uri, 'GTestCase', Atheist.StatusID.FAIL)

        self.test_start = Atheist.TestInfo(
            uri, 'GTestCase', 'TestRunOk', Atheist.StatusID.START)
        self.test_ok = Atheist.TestInfo(
            uri, 'GTestCase', 'TestRunOk', Atheist.StatusID.OK)
        self.test_fail = Atheist.TestInfo(
            uri, 'GTestCase', 'TestRunOk', Atheist.StatusID.FAIL)

    def custom_setup(self, uri):
        self.build_infos(uri)
        self.reporter = Spy(Atheist.Reporter)
        reporter_proxy = self.get_reporter_proxy(self.reporter)
        self.worker = Worker(uri, reporter_proxy)
        self.worker.fs = self.fs

    def test_notify_when_all_ok(self):
        # given
        uri = "existing-file"
        self.fs.createfile(uri)
        self.custom_setup(uri)

        # when
        self.worker.run()

        # then
        assert_that(self.reporter.notifySuiteInfo,
                    called_with(self.suite_start))

        assert_that(self.reporter.notifySuiteInfo,
                    called_with(self.suite_stop))

    def test_notify_with_missing_test_file(self):
        # given
        uri = "missing-file"
        self.custom_setup(uri)

        # when
        self.worker.run()

        # then
        assert_that(self.reporter.notifySuiteInfo,
                    called_with(self.suite_error))


class TestGTestWorker(TestBase):
    def custom_setup(self, uri):
        self.reporter = Spy(Atheist.Reporter)
        reporter_proxy = self.get_reporter_proxy(self.reporter)
        self.worker = GTestWorker(uri, reporter_proxy)

    def test_notify_suite_output_when_fail(self):
        uri = os.path.join(os.path.dirname(__file__), "test-fail")
        self.custom_setup(uri)

        self.worker.run()

        assert_that(self.reporter.notifySuiteOutput, called())

    def test_notify_suite_output_when_ok(self):
        uri = os.path.join(os.path.dirname(__file__), "test-ok")
        self.custom_setup(uri)

        self.worker.run()

        assert_that(self.reporter.notifySuiteOutput, called())
