# -*- mode:python; coding:utf-8; tab-width:4 -*-

import os
from unittest import TestCase

import Ice
from fs.memoryfs import MemoryFS
from fs.osfs import OSFS

from atheist2.manager import Manager
from atheist2.reporter import Reporter, ConsoleReporter
from atheist2.worker.base import Worker, WorkerFactoryBase
import Atheist
from Atheist import StatusID as st


test_ok = '''\
import unittest
class Case1(unittest.TestCase):
    def test_ok(self):
        pass
'''

test_fail = '''\
import unittest
class Case1(unittest.TestCase):
    def test_fail(self):
        self.fail()
'''


class MemoryFS_Mixin(object):
    def setUp(self):
        self.fs = MemoryFS()
        WorkerFactoryBase.fs = self.fs
        Worker.fs = self.fs
        Manager.fs = self.fs

        self.reporter = Reporter()
        self.manager = Manager()
        self.manager.set_reporter(self.reporter)


class Test_exec_single_uri(MemoryFS_Mixin, TestCase):
    def test_ok_test(self):
        uri = '/ok.py'
        self.fs.createfile(uri, test_ok)
        self.manager.run([uri])
        self.assertEquals(self.reporter.getStatus(), st.OK)

    def test_error_test(self):
        uri = '/missing.py'
        self.manager.run([uri])
        self.assertEquals(self.reporter.getStatus(), st.ERROR)

    def test_fail_test(self):
        uri = '/fail.py'
        self.fs.createfile(uri, test_fail)
        self.manager.run([uri])
        self.assertEquals(self.reporter.getStatus(), st.FAIL)


class Test_exec_several_uris(MemoryFS_Mixin, TestCase):
    def test_2_ok(self):
        # given
        uris = ['/ok.py', '/ok2.py']
        for fname in uris:
            self.fs.createfile(fname, test_ok)

        # when
        self.manager.run(uris)

        # then
        self.assertEquals(self.reporter.getStatus(), st.OK)

    def test_1_ok_1_fail(self):
        # given
        uris = ['/ok.py', '/fail.py']
        self.fs.createfile(uris[0], test_ok)
        self.fs.createfile(uris[1], test_fail)

        # when
        self.manager.run(uris)

        # then
        self.assertEquals(self.reporter.getStatus(), st.FAIL)
        self.assertEquals(len(self.reporter.suites), 2)


class Test_exec_recursively_uris(MemoryFS_Mixin, TestCase):
    def test_one_level(self):
        # given
        uri = '/test'
        test = uri + '/ok.py'
        self.fs.makedir(uri)
        self.fs.createfile(test, test_ok)

        # when
        self.manager.run([uri])

        # then
        self.assertEquals(self.reporter.getStatus(), st.OK)

    def test_two_level(self):
        # given
        uri = '/test'
        self.fs.makedir('/test/unit', recursive=True)

        tests = ['test/ok.py', 'test/unit/ok.py']
        for fname in tests:
            self.fs.createfile(fname, test_ok)

        # when
        self.manager.run([uri])

        # then
        self.assertEquals(self.reporter.getStatus(), st.OK)
        self.assertEquals(len(self.reporter.suites), 2)


class Test_gtest(TestCase):
    def setUp(self):
        fs = OSFS("/")
        WorkerFactoryBase.fs = fs
        Worker.fs = fs

        self.reporter = ConsoleReporter()
        self.manager = Manager()

        ic = Ice.initialize()
        adapter = ic.createObjectAdapterWithEndpoints("adapter", "tcp")
        reporter_prx = adapter.addWithUUID(self.reporter)
        reporter_prx = Atheist.ReporterPrx.uncheckedCast(reporter_prx)
        adapter.activate()

        self.manager.set_reporter(reporter_prx)

    def test_gtest_failure(self):
        uri = os.path.join(os.getcwd(),
                           "test/integration/gtest-worker/test-fail")
        print uri
        self.manager.run([uri])
        self.assertEquals(self.reporter.getStatus(), st.ERROR)

    def test_gtest_ok(self):
        uri = os.path.join(os.getcwd(),
                           "test/integration/gtest-worker/test-ok")
        self.manager.run([uri])
        self.assertEquals(self.reporter.getStatus(), st.OK)
