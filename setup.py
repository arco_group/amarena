#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-

# Copyright (C) 2009,2012 David Villa Alises, Oscar Aceña
#
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA


def get_version():
    with file("debian/changelog") as src:
        line = src.readline()
        return line.split()[1].strip("()")


from distutils.core import setup

setup(name         = 'atheist2',
      version      = get_version(),
      description  = 'Atheist2 is a general purpose test framework written in Python.',
      author       = 'David Villa Alises',
      author_email = '<David.Villa@uclm.es>',
      url          = 'https://bitbucket.org/arco_group/atheist2',
      license      = 'GPL v2 or later',
      packages     = ['atheist2', 'atheist2/worker', 'atheist2/reporter'],
      package_data = {'atheist2/worker': ['gtest/*']},
      )
