#!/usr/bin/python
# -*- mode:python; coding:utf-8; tab-width:4 -*-

import os
import sys
import logging

import Ice

cwd = os.path.dirname(__file__)
sys.path.insert(0, os.path.join(cwd, ".."))
import atheist2

logging.basicConfig(format="%(message)s", level="DEBUG")


class ReporterServer(Ice.Application):
    def run(self, args):
        adapter = self.create_adapter()
        self.servant = self.create_servant()
        oid = self.get_servant_oid()

        proxy = adapter.add(self.servant, oid)
        print "Reporter available at '{0}'".format(proxy)

        self.wait()

    def create_adapter(self):
        ic = self.communicator()
        endps = ic.getProperties().getProperty("ReporterAdapter.Endpoints")
        if not endps:
            endps = "tcp -h localhost -p 6677"

        adapter = ic.createObjectAdapterWithEndpoints("ReporterAdapter", endps)
        adapter.activate()
        return adapter

    def create_servant(self):
        from atheist2.reporter import ConsoleReporter
        return ConsoleReporter()

    def get_servant_oid(self):
        ic = self.communicator()
        oid = ic.getProperties().getProperty("Reporter.Identity")
        if not oid:
            oid = "Reporter"
        return ic.stringToIdentity(oid)

    def wait(self):
        import time
        while True:
            if self.servant.suites:
                self.servant.finish()
                self.servant.clear()
            time.sleep(1)

        # self.shutdownOnInterrupt()
        # self.communicator().waitForShutdown()


if __name__ == "__main__":
    srv = ReporterServer()
    sys.exit(srv.main(sys.argv))





