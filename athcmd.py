#!/usr/bin/python
# -*- coding:utf-8; mode:python -*-

import sys
import logging

from atheist2.manager import Manager

logging.basicConfig(format="%(message)s", level="DEBUG")


if __name__ == "__main__":
    manager = Manager()
    manager.create_default_reporter()
    sys.exit(manager.main(sys.argv[1:]))

