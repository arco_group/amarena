# -*- mode: python; coding: utf-8 -*-

import logging
import os
from collections import OrderedDict

import Atheist
from Atheist import StatusID as st
from Atheist import CaseInfo


class Suite(OrderedDict):
    def __init__(self, info):
        OrderedDict.__init__(self)
        self.name = info.name
        self.status = info.status
        self.output = {}

    def get_output(self, type):
        return self.output.get(type, "")

    def set_output(self, data):
        self.output[data.type] = self.get_output(data.type) + data.data

    def get_case(self, name):
        try:
            retval = self[name]
        except KeyError:
            retval = Case(CaseInfo(self.name, name, st.UNKNOWN))
            self[name] = retval

        return retval

    def update_case(self, case):
        try:
            self[case.name].status = case.status
        except KeyError:
            self[case.name] = case


class Case(OrderedDict):
    def __init__(self, info):
        OrderedDict.__init__(self)
        self.name = info.name
        self.status = info.status

    def get_test(self, name):
        return self[name]

    def update_test(self, test):
        try:
            self[test.name].status = test.status
        except KeyError:
            self[test.name] = test


class DebugReporter(Atheist.Reporter):
    def notifySuiteInfo(self, info, current=None):
        print 'Suite {0}: {1}'.format(info.name, info.status)

    def notifySuiteOutput(self, data, current=None):
        print 'SuiteOut {0}: {1} {2}'.format(data.name, data.type, data.data)

    def notifyCaseInfo(self, info, current=None):
        print 'Case  {0}.{1}: {2}'.format(info.suiteName, info.name, info.status)

    def notifyTestInfo(self, info, current=None):
        print 'Test  {0}.{1}.{2}: {3}'.format(
            info.suiteName, info.caseName, info.name, info.status)


class Reporter(Atheist.Reporter):
    def __init__(self):
        self.clear()

    def finish(self):
        pass

    def clear(self):
        self.suites = OrderedDict()

    def notifySuiteInfo(self, info, current=None):
        try:
            suite = self.suites[info.name]
            suite.status = info.status
        except KeyError:
            self.suites[info.name] = Suite(info)

        if info.status in [st.STOP, st.ERROR]:
            self.finish()

    def notifySuiteOutput(self, data, current=None):
        try:
            suite = self.suites[data.name]
            suite.set_output(data)
        except KeyError:
            raise Atheist.UnknownSuiteException(data.name)

    def notifyCaseInfo(self, info, current=None):
        try:
            suite = self.suites[info.suiteName]
        except KeyError:
            suite = Suite(info)
            self.suites[info.suiteName] = suite

        suite.update_case(Case(info))

    def notifyTestInfo(self, info, current=None):
        suite = self.suites[info.suiteName]
        case = suite.get_case(info.caseName)
        case.update_test(info)

    def getStatus(self, current=None):
        for suite in self.suites.values():
            if suite.status == st.ERROR:
                return st.ERROR

            cases_status = self.get_suite_children_status(suite)
            if cases_status != st.OK:
                return cases_status

        return st.OK

    def get_suite_children_status(self, suite):
        for case in suite.values():
            for test in case.values():
                if test.status != st.OK:
                    return st.FAIL
        return st.OK


NORMAL = "\033[00m"
GREEN  = "\033[01;32m"
YELLOW = "\033[01;33m"
RED    = "\033[01;31m"


class ConsoleReporter(Reporter, Atheist.Reporter):
    def __init__(self):
        Reporter.__init__(self)

    def finish(self):
        out = self.render()
        logging.info(YELLOW + "-- Report " + "-" * 20 + NORMAL)
        for line in out.splitlines():
            logging.info(line)

    def render(self):
        retval = ""
        for suite in self.suites.values():
            retval += self.render_suite(suite)
        return retval

    def render_suite(self, suite):
        retval = self.get_suite_info(suite)
        retval += self.get_suite_output(suite)
        for case in suite.values():
            retval += self.render_case(case)
        return retval

    def render_case(self, case):
        retval = self.get_case_info(case)
        for test in case.values():
            retval += self.get_test_info(test)
        return retval

    def get_suite_info(self, suite):
        assert isinstance(suite, Suite)
        return "[ {suite.status:^7} ] TestSuite: {suite.name}\n".format(
            **locals())

    def get_suite_output(self, suite):
        retval = ""
        for line in suite.get_output(Atheist.OutputType.STDOUT).splitlines():
            retval += "  [out] " + line + "\n"
        for line in suite.get_output(Atheist.OutputType.STDERR).splitlines():
            retval += "  [err] " + line + "\n"

        return retval

    def get_case_info(self, case):
        assert isinstance(case, Case)
        return "[ {case.status:^7} ] - TestCase: {case.name}\n".format(
            **locals())

    def get_test_info(self, test):
        color = GREEN if test.status == Atheist.StatusID.OK else RED
        assert isinstance(test, Atheist.TestInfo)
        return "[ {0}{test.status:^7}{1} ] -- Test: {test.name}\n".format(
            color, NORMAL, **locals())


class ReporterFactory(object):
    @classmethod
    def create(cls, key):
        return ConsoleReporter()
