# -*- coding:utf-8; tab-width:4; mode:python -*-

import os
import logging
import argparse

import Ice
from fs.osfs import OSFS
import fs.osfs.watch  # fs.osfs.watch.OSFSWatchMixin.close import bug


from atheist2.worker import LocalWorkerFactory, CantHandleURI
from atheist2.reporter import ReporterFactory
import Atheist


class Broker:
    def __init__(self):
        self.ic = None
        self.adapter = None

    def _setup(self):
        self.ic = Ice.initialize()
        self.adapter = self.ic.createObjectAdapterWithEndpoints(
            'adapter', 'tcp')

        self.adapter.activate()

    def add(self, servant):
        if self.ic is None:
            self._setup()

        prx = self.adapter.addWithUUID(servant)
        return Atheist.ReporterPrx.uncheckedCast(prx)


class Manager:
    root_fs = OSFS('/')

    def __init__(self):
        self.reporter = None
        self.broker = Broker()

    def main(self, argv):
        parser = argparse.ArgumentParser("Atheist Manager")
        parser.add_argument('uri', help="URI to tests")
        args = parser.parse_args(argv)

        return self.run([args.uri])

    def set_reporter(self, reporter):
        self.reporter = reporter

    def create_default_reporter(self):
        if self.reporter is not None:
            return

        reporter_servant = ReporterFactory.create("console")
        self.set_reporter(self.broker.add(reporter_servant))

    def run(self, uris):
        assert(isinstance(uris, list))
        while uris:
            uri = uris.pop(0)
            try:
                uri = os.path.abspath(uri)
                self.run_uri(uri)
            except CantHandleURI:
                if self.root_fs.isdir(uri):
                    uris[0:0] = [os.path.join(uri, x) for x in
                                 self.root_fs.listdir(uri)]
                    continue

                logging.error("No worker available for given URI")
                info = Atheist.SuiteInfo(uri, Atheist.StatusID.ERROR)
                self.reporter.notifySuiteInfo(info)

    def run_uri(self, uri):
        worker = LocalWorkerFactory.create(uri, self.reporter)
        logging.info("URI: '{0}', to handle with {1}".format(uri, worker.worker_type))
        worker.run()



