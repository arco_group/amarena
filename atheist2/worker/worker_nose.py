# -*- coding:utf-8; tab-width:4; mode:python -*-

import sys
import os
import logging

import nose

from commodity.os_ import SubProcess

from Atheist import StatusID as st
from atheist2.worker.base import Worker, WorkerFactoryBase


class NoseFactory(WorkerFactoryBase):
    def canHandle(self, uri, current=None):
        if (self.fs.isfile(uri) and uri.endswith('.py')):
            return True

        if self.fs.isdir(uri):
            pyfiles = self.fs.listdir(uri, wildcard="*.py", files_only=True)
            if pyfiles:
                return True

        logging.debug("PyUnit can not handle %s", uri)
        return False

    def create(self, uri, reporter):
        return NoseWorker(uri, reporter)


class NoseWorker(Worker):
    worker_type = "nose"

    def do_run(self):
        path = os.path.join(os.path.dirname(__file__), 'nose')
        cmd = "{0}/nose_launcher.py --cwd {1} {2} '{3}'".format(
            path, '.', self.uri, self.reporter)
        ps = SubProcess(cmd, stderr=sys.stderr)
        ps.wait()

        return ps.returncode == 0
