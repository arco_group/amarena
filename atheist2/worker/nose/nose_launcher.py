#!/usr/bin/python
# -*- mode:python; coding:utf-8; tab-width:4 -*-

import sys
import os
import argparse

import Ice
import nose

Ice.loadSlice('atheist.ice')
import Atheist
from Atheist import StatusID as st


class AtheistTestResult(nose.core.TextTestResult):
    def __init__(self, reporter, *args, **kargs):
        self.reporter = reporter
        super(AtheistTestResult, self).__init__(*args, **kargs)
        self.current_case = None

    def get_case_method(self, test):
        suite, id_, test = test.address()
        if test is None:
            return "unknown", "unknown"

        case, method = test.split('.')
        return case, method

    def _notify(self, test, status):
        case, method = self.get_case_method(test)
        self._notify_change_case(case)

        testinfo = Atheist.TestInfo("suite", case, method, status)
        self.reporter.notifyTestInfo(testinfo)

    def _notify_change_case(self, case):
        if case == self.current_case:
            return

        caseinfo = Atheist.CaseInfo("suite", case, st.START)
        self.reporter.notifyCaseInfo(caseinfo)
        self.current_case = case

    def addSuccess(self, test):
        self._notify(test, st.OK)

    def addFailure(self, test, err):
        self._notify(test, st.FAIL)

    def addError(self, test, err):
        self._notify(test, st.ERROR)

    def addExpectedFailure(self, test, *args):
        raise NotImplementedError

    def startTest(self, test):
        self._notify(test, st.START)

    def stopTest(self, test):
        # self._notify(test, st.STOP)
        pass

    def printErrors(self, *args):
#        print "errors:_", args
        pass

    def printLabel(self, *args):
#        print "label:_", args
        pass

    def printSummary(self, *args):
        pass


class AtheistTestRunner(nose.core.TextTestRunner):
    def __init__(self, reporter):
        self.reporter = reporter
        super(AtheistTestRunner, self).__init__()

    def _makeResult(self):
        return AtheistTestResult(self.reporter,
                                 self.stream,
                                 self.descriptions,
                                 self.verbosity,
                                 self.config)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('testsuite', help='path to the testsuite file')
    parser.add_argument('reporter',  help="Remote reporter proxy")
    parser.add_argument('--cwd',     default=".")
    config = parser.parse_args(sys.argv[1:])

    print config

    ic = Ice.initialize()
    os.chdir(config.cwd)

    proxy = ic.stringToProxy(config.reporter)
    reporter = Atheist.ReporterPrx.uncheckedCast(proxy)

    loader = nose.loader.TestLoader()
    suite = loader.loadTestsFromName(config.testsuite)

    runner = AtheistTestRunner(reporter)
    runner.run(suite)


if __name__ == '__main__':
    main()
