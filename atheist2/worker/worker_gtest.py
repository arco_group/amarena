# -*- coding:utf-8; tab-width:4; mode:python -*-

import os
import io
import logging

from commodity.os_ import SubProcess

from Atheist import OutputType
from atheist2.worker.base import Worker, WorkerFactoryBase


class GTestFactory(WorkerFactoryBase):
    def canHandle(self, uri, current=None):
        if (self.fs.isdir(uri) and
            self.fs.isfile(os.path.join(uri, "atheist.mk"))):
            return True

        logging.debug("GTest can not handle %s", uri)
        return False

    def create(self, uri, reporter):
        return GTestWorker(uri, reporter)


class GTestWorker(Worker):
    worker_type = "gtest"

    def do_run(self):
        if not self.compileTests():
            return False
        return self.launchTests()

    def compileTests(self):
        gtest_make = os.path.join(os.path.dirname(__file__), "gtest/Makefile")
        cmd = ['make', '-f', gtest_make, "-C", self.uri,
               'atheist-gtest-launcher']
        return self.execCmd(cmd)

    def launchTests(self):
        gtest_make = os.path.join(os.path.dirname(__file__), "gtest/Makefile")
        cmd = ['make', '-f', gtest_make, "-C", self.uri,
               'atheist-run', 'OBSERVER_PROXY="{0}"'.format(self.reporter)]
        return self.execCmd(cmd)

    def execCmd(self, cmd):
        stdout = io.BytesIO()
        stderr = io.BytesIO()
        ps = SubProcess(cmd, stdout=stdout, stderr=stderr)
        ps.wait()

        logging.debug("exec: '{0}'".format(cmd))

        if ps.returncode:
            self.notifySuiteOutput(OutputType.STDERR, stderr.getvalue())
            return False

        self.notifySuiteOutput(OutputType.STDOUT, stdout.getvalue())
        return True
