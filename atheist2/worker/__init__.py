# -*- coding:utf-8; tab-width:4; mode:python -*-

from worker_gtest import GTestFactory
from worker_nose import NoseFactory


class CantHandleURI(Exception):
    pass


class LocalWorkerFactory:
    factories = {"pyunit": NoseFactory(),
                 "gtest":  GTestFactory()}

    @classmethod
    def create(cls, uri, reporter):
        factory = cls.get_factory_for_uri(uri)
        return factory.create(uri, reporter)

    @classmethod
    def get_factory_for_uri(cls, uri):
        for factory in cls.factories.values():
            if factory.canHandle(uri):
                return factory

        raise CantHandleURI(uri)
