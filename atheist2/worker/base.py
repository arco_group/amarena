# -*- mode: python; coding: utf-8 -*-

import os
import logging

from fs.osfs import OSFS

import Atheist


class Worker(Atheist.Worker):
    fs = OSFS('/')

    def __init__(self, uri, reporter):
        self.uri = uri
        self.reporter = reporter

    def run(self, current=None):
        if not self.fs.exists(self.uri):
            logging.error("Worker: Suite not found: %s", self.uri)
            self.notifyError()
            self.notifyStop()
            return

        self.notifyStart()

        if not self.do_run():
            logging.error("Worker: Error on suite: %s", self.uri)
            self.notifyError()

        self.notifyStop()

    def do_run(self):
        return True

    def notifyStart(self):
        self.notifySuiteStatus(Atheist.StatusID.START)

    def notifyStop(self):
        self.notifySuiteStatus(Atheist.StatusID.STOP)

    def notifyError(self):
        self.notifySuiteStatus(Atheist.StatusID.ERROR)

    def notifySuiteStatus(self, status):
        info = Atheist.SuiteInfo(self.uri, status)
        self.reporter.notifySuiteInfo(info)

    def notifyCaseStatus(self, casename, status):
        info = Atheist.CaseInfo(self.uri, casename, status)
        self.reporter.notifyCaseInfo(info)

    def notifyTestStatus(self, casename, testname, status):
        info = Atheist.TestInfo(self.uri, casename, testname, status)
        self.reporter.notifyTestInfo(info)

    def notifySuiteOutput(self, type, out):
        data = Atheist.SuiteOutputData(self.uri, type, out)
        self.reporter.notifySuiteOutput(data)


class WorkerFactoryBase(Atheist.WorkerFactory):
    fs = OSFS('/')
