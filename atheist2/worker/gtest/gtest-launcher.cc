// -*- mode: c++; coding: utf-8 -*-

#include <Ice/Ice.h>
#include <gtest/gtest.h>

#include "gtest-listener.h"
#include "atheist.h"


using namespace testing;
using namespace Atheist;

int __argc;
char** __argv;

class GoogleTestLauncher {
public:

  int run(int argc, char** argv) {
    GTEST_FLAG(catch_exceptions) = false;
    InitGoogleTest(&argc, argv);
    InitIce(argc, argv);

    if (argc < 2) {
      std::cerr << "[GTestLauncher] ERROR: Observer's Proxy not specified!\n"
		<< "  The main cause for this is an incorrect use of GTestLauncher.\n"
		<< "  Did you give the Observer's Proxy ad command line argument?\n";
      return 1;
    }

    setupAtheistObserver(std::string(argv[1]));
    setupGoogleTestListener();

    return RUN_ALL_TESTS();
  }

  void InitIce(int argc, char** argv) {
    ic = Ice::initialize(argc, argv);
  }

  void setupGoogleTestListener() {
    TestEventListeners& listeners = UnitTest::GetInstance()->listeners();
    delete listeners.Release(listeners.default_result_printer());
    listeners.Append(new ReporterForward(observer));
  }

  void setupAtheistObserver(std::string strProxy) {
    Ice::ObjectPrx proxy = ic->stringToProxy(strProxy);
    observer = ReporterPrx::uncheckedCast(proxy);
  }

private:
  Ice::CommunicatorPtr ic;
  ReporterPrx observer;
};


int main(int argc, char** argv) {

    /* save argv for future use in tests */
    __argc = argc;
    __argv = (char**) malloc(argc * sizeof(char**));
    for (int i=0; i<argc; i++) {
	__argv[i] = (char*) malloc(strlen(argv[i]) * sizeof(char) + 1);
	strcpy(__argv[i], argv[i]);
    }

    GoogleTestLauncher worker;
    return worker.run(argc, argv);
}

