// -*- mode: c++; coding: utf-8 -*-

#include <gtest/gtest.h>

#include "atheist.h"


class ReporterForward: public ::testing::EmptyTestEventListener {
  Atheist::ReporterPrx observer;

public:
  ReporterForward(::Atheist::ReporterPrx prx);
  void OnTestEnd(const ::testing::TestInfo& test_info);
  void OnTestCaseStart(const ::testing::TestCase& test_case);
  void OnTestStart(const ::testing::TestInfo& test_info);
  void OnTestCaseEnd(const ::testing::TestCase& test_case);
};


