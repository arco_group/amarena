// -*- mode: c++; coding: utf-8 -*-

#include "gtest-listener.h"


ReporterForward::ReporterForward(::Atheist::ReporterPrx observer) :
    observer(observer) {
}

void
ReporterForward::OnTestCaseStart(const ::testing::TestCase& cinfo) {
    // std::cout << "OnTestCaseStart" << std::endl;

    Atheist::CaseInfo ainfo = {
	ATHEIST_SUITE_NAME,
	cinfo.name(),
	::Atheist::START
    };

    observer->notifyCaseInfo(ainfo);
}

void
ReporterForward::OnTestStart(const ::testing::TestInfo& tinfo) {
    // std::cout << "OnTestStart" << std::endl;

    Atheist::TestInfo ainfo = {
	ATHEIST_SUITE_NAME,
	tinfo.test_case_name(),
	tinfo.name(),
	::Atheist::START
    };

    observer->notifyTestInfo(ainfo);
}

void
ReporterForward::OnTestEnd(const ::testing::TestInfo& tinfo) {
    // std::cout << "OnTestEnd" << std::endl;

    const ::testing::TestResult* result = tinfo.result();
    Atheist::TestInfo ainfo = {
	ATHEIST_SUITE_NAME,
	tinfo.test_case_name(),
	tinfo.name(),
	result->Failed() ? ::Atheist::FAIL : ::Atheist::OK
    };

    observer->notifyTestInfo(ainfo);
}

void
ReporterForward::OnTestCaseEnd(const ::testing::TestCase& cinfo) {
    // std::cout << "OnTestCaseEnd" << std::endl;

    Atheist::CaseInfo ainfo = {
	ATHEIST_SUITE_NAME,
	cinfo.name(),
	::Atheist::STOP
    };

    observer->notifyCaseInfo(ainfo);
}
