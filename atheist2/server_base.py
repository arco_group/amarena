# -*- mode: python; coding: utf-8 -*-

import sys
import Ice
import argparse


class ServerApp(Ice.Application):

    def __init__(self, description):
        self.description = description
        Ice.Application.__init__(self)

    def run(self, args):
        try:
            self.args = self.parse_args(args[1:])
            self.ic = self.communicator()
        except SystemExit:
            return -1

        self.create_adapter()
        self.add_servant()
        self.wait()

    def add_options(self, parser):
        pass

    def build_servant(self):
        raise NotImplementedError

    def parse_args(self, args):
        parser = argparse.ArgumentParser(description=self.description)
        parser.add_argument('-e','--endpoints',
                            default='tcp -h localhost -p 1234',
                            help='endpoints to use on adapter')
        parser.add_argument('-i', '--identity', default='ObjectID',
                            help='object identity')
        parser.add_argument('-d', '--datagram', action='store_true',
                            help='use datagram endpoint')
        self.add_options(parser)
        return parser.parse_args(args)

    def create_adapter(self):
        self.oa = self.ic.createObjectAdapterWithEndpoints(
            'Adapter', self.args.endpoints)
        self.oa.activate()

    def add_servant(self):
        srv = self.build_servant()
        oid = self.ic.stringToIdentity(self.args.identity)
        prx = self.oa.add(srv, oid)

        if self.args.datagram:
            prx = prx.ice_datagram()

        print "'{0}'".format(prx)

    def wait(self):
        self.shutdownOnInterrupt()
        self.ic.waitForShutdown()


