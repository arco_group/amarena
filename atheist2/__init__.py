# -*- mode:python; coding:utf-8; tab-width:4 -*-

import os
import Ice

cwd = os.path.dirname(__file__)
atheist_slice = os.path.join(cwd, "../atheist.ice")
Ice.loadSlice(atheist_slice)

import Atheist as Slice

